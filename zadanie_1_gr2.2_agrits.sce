//Matlab applications in physics
//Author: Artur Kasza
//Technical Physics

//Defining functions to be used in solutions

function digit=digitAfterDot(number, place)
    digit = modulo(floor(number*10^place), 10)
endfunction

function days=daysSince(day, month, year)
    days = floor(now()-datenum(year, month, day))
endfunction

//Defining constants to be used in solutions
%h = 6.62607015e-34 //Planck constant according to NIST
%NA = 6.02214076e23 //Value of Avogadro constant and number according to NIST
%Rz = 6378.137 //Earth's equatorial radius in km according to NASA
%ethanolAtoms = 2+5+2 //Number of atoms in one ethanol particle - C2H5OH
%micro = 1e-6
//Exercise 1
%hBar = %h/(2*%pi)

//Exercise 2
result2 = sind(30/exp(1))

//Exercise 3
result3 =  hex2dec("00123d3")/(2.455e23)

//Exercise 4
result4 = sqrt(exp(1)-%pi)

//Exercise 5
result5 = digitAfterDot(%pi, 10)

//Exercise 6
result6 = daysSince(22, 07, 1998)

//Exercise 7
result7 = atan(exp(sqrt(7)/2 - log(%Rz/1e5))/hex2dec("aabb"))

//Exercise 8
result8 = %ethanolAtoms * %NA*(1/5)*%micro 
//Number of atoms in one ethanol particle multiplied by number of particles in 0.2 of mole - 1/5 times Acogadro number

//Exercise 9
coalAtoms = 2/%ethanolAtoms * result8
coalThirteenAtoms = coalAtoms / 100 
promile = coalThirteenAtoms/result8 * 1000


